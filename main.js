require("dotenv").config();
const express = require("express"); // call express
const app = express(); // define our app using express
const bodyParser = require("body-parser");
const cors = require("cors");

const routes = require("./routes/router.js");

const port = process.env.PORT; // set our port

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use("/", routes);

app.listen(port);
console.log("Magic happens on port " + port);
