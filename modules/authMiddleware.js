const checkToken = require("./auth").checkToken;
const sendInfo = require("./addBlock").getInfo;

const authMiddleware = (req, res, next) => {
  if (
    req.hasOwnProperty("headers") &&
    req.headers.hasOwnProperty("authorization")
  ) {
    checkToken(req.headers["authorization"])
      .then(decoded => {
        const amount = req.body.amount;
        const login = decoded.login;
        const timestamp = new Date().getTime();
        const info = { amount: amount, login: login, timestamp: timestamp };
        sendInfo(info)
          .then(message => {
            res.status(200).json({
              message: message
            });
            next();
          })
          .catch(err => {
            res.status(500).json({
              message: err
            });
          });
      })
      .catch(err => {
        res.status(400).json({ message: err });
      });
  } else {
    res.status(400).json({ message: "No token sorry" });
  }
};

module.exports.authMiddleware = authMiddleware;
