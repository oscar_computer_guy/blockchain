const blockchain = require("./blockchain");
const addRecord = require("./addRecord").addRecord;

const addGenesis = data => {
  return new Promise((resolve, reject) => {
    blockchain
      .block(true, data)
      .then(function(object) {
        addRecord(object)
          .then(function() {
            resolve();
          })
          .catch(function(err) {
            console.log(err);

            reject(err);
          });
      })
      .catch(function(err) {
        console.log(err);
        reject(err);
      });
  });
};

module.exports.addGenesis = addGenesis;
