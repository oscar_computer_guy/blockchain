const blockchain = require("./blockchain");
const addRecord = require("./addRecord").addRecord;

const addNew = data => {
  return new Promise((resolve, reject) => {
    blockchain
      .block(false, data)
      .then(function(object) {
        addRecord(object)
          .then(function() {
            resolve();
          })
          .catch(function(err) {
            reject(err);
          });
      })
      .catch(function(err) {
        console.log(err);
        reject(err);
      });
  });
};

module.exports.addNew = addNew;
