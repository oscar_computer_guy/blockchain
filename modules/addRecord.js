const Blocks = require("../models/block.model");

const addRecord = object => {
  return new Promise((resolve, reject) => {
    const genBlock = new Blocks({
      index: object.index,
      timestamp: object.timestamp,
      data: object.data,
      previousHash: object.previousHash,
      hash: object.hash
    });
    genBlock
      .save()
      .then(result => {
        console.log("record created");
        resolve();
      })
      .catch(err => {
        console.log(err);
        reject(err);
      });
  });
};

module.exports.addRecord = addRecord;
