const jwt = require("jsonwebtoken");

const checkToken = token => {
  return new Promise((resolve, reject) => {
    try {
      jwt.verify(token, process.env.JWT_SECRET, function(error, decoded) {
        if (error) {
          reject(error.message);
        } else {
          resolve(decoded);
        }
      });
    } catch (err) {
      reject("Error verifying the token");
    }
  });
};

module.exports.checkToken = checkToken;
