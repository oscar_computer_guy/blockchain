const connect = require("./dbConnect");
const Blocks = require("../models/block.model");
const mongoose = require("mongoose");
const addGenesis = require("./addGenesis").addGenesis;
const addNew = require("./addNew").addNew;
// const blockchain = require("./blockchain");

const getInfo = data => {
  return new Promise((resolve, reject) => {
    Blocks.find()
      .exec()
      .then(function(blocks) {
        if (blocks.length == 0) {
          addGenesis(data)
            .then(function() {
              resolve("Genesis Block added");
            })
            .catch(function(err) {
              reject(err);
            });
        } else {
          addNew(data)
            .then(function() {
              resolve("New Block added");
            })
            .catch(function(err) {
              reject(err);
            });
        }
      })
      .catch(err => {
        reject(err);
      });
  });
};

module.exports.getInfo = getInfo;
