const express = require("express");
const authMiddleware = require("../modules/authMiddleware").authMiddleware;

const router = express.Router();

router.get("/", function(req, res) {
  res.json({ message: "Welcome to the blockchain practice" });
});

router.post("/newBlock", authMiddleware);

module.exports = router;
